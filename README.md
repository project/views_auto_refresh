# Views: Auto Refresh

A module to refresh a view after a specified time interval or
when triggered by an event.

Includes advanced options to avoid reloading the whole view,
and to avoid causing a full Drupal bootstrap at each refresh.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/views_auto_refresh).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/views_auto_refresh).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

No special requirements at this time.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Create a new page view or alter existing view.
2. Add a new header of type Global: Auto Refresh.
3. Set the interval time for view refresh.
4. Disable cache on the view (section: advanced/other).
5. Enable `"Use Ajax"` on the view (section: advanced/other).
6. Save view and now your view should be refreshing.


## Maintainers

- George Anderson - [geoanders](https://www.drupal.org/u/geoanders)
- Michael O'Hara - [mikeohara](https://www.drupal.org/u/mikeohara)
